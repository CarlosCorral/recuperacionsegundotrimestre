<?php 
class Alimentacion extends Producto{
    private $mes,$anyo;
    public function __construct($codigo,$precio,$nombre,$categoria,$mes,$anyo){
        parent::__construct($codigo,$precio,$nombre,$categoria);
        $this->mes=$mes;
        $this->anyo=$anyo;
    }

    public function mostrar(){
        return parent::mostrar()." mes ". $this->mes." año ".$this->anyo;
    }


    /**
     * Get the value of mes
     */ 
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set the value of mes
     *
     * @return  self
     */ 
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get the value of anyo
     */ 
    public function getAnyo()
    {
        return $this->anyo;
    }

    /**
     * Set the value of anyo
     *
     * @return  self
     */ 
    public function setAnyo($anyo)
    {
        $this->anyo = $anyo;

        return $this;
    }
}
?>