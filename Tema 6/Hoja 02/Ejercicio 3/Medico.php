<?php 
abstract class Medico{
    protected $nombre,$edad,$turno;
    public function __construct($nombre,$edad,$turno){
        $this->nombre=$nombre;
        $this->edad=$edad;
        $this->turno=$turno;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function getEdad(){
        return $this->edad;
    }
    public function getTurno(){
        return $this->turno;
    }
public function mostrar(){
    return "Nombre :".$this->nombre." Edad: ".$this->edad." Turno:".$this->turno;
}


}
?>