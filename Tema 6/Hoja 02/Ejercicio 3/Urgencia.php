<?php 
class Urgencia extends Medico{
    private $unidad;
    public function __construct($nombre,$edad,$turno,$unidad){
        parent::__construct($nombre,$edad,$turno);
        $this->unidad=$unidad;
    }

    public function mostrar(){
        return parent::mostrar()." unidad ". $this->unidad;
    }

}
?>