<?php 
class Familia extends Medico{
    private $num_pacientes;
    public function __construct($nombre,$edad,$turno,$num_pacientes){
        parent::__construct($nombre,$edad,$turno);
        $this->num_pacientes=$num_pacientes;
    }
    public function getNum_pacientes(){
        return  $this->num_pacientes;
    }


    public function mostrar(){
        return parent::mostrar()." num_pacientes ". $this->num_pacientes;
    }

}
?>