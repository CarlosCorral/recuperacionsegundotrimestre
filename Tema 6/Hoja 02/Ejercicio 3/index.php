<?php
include "Medico.php";
include "Urgencia.php";
include "Familia.php";

$medicos[]=new Familia("Carlos",80,"mañana",10);
$medicos[]=new Familia("Luis",25,"tarde",15);
$medicos[]=new Familia("Javier",65,"tarde",40);
$medicos[]=new Urgencia("Maria",27,"mañana","Primeros auxilios");
$medicos[]=new Urgencia("Andrea",82,"mañana","Primeros auxilios");
$medicos[]=new Urgencia("Lucia",67,"tarde","Primeros auxilios");
echo "Medicos de más de 60 años de urgencias con turno de tarde";
foreach($medicos as $medico){
    if($medico instanceof Urgencia && $medico->getTurno() =="tarde"&& $medico->getEdad() >60){
        echo "<br>".$medico->mostrar();
    }   
}
echo "<br>Medicos familiares con mas de 12 pacientes";
$numeroLimite=12;
foreach($medicos as $medico){
    if($medico instanceof Familia && $medico->getNum_pacientes() >=$numeroLimite){
        echo "<br>".$medico->mostrar();
    }   
}
?>