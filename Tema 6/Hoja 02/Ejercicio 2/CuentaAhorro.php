<?php 
class CuentaAhorro extends Cuenta{
    protected $apertura,$interes;
    public function __construct($numero,$titular,$saldo,$apertura,$interes){
        parent::__construct($numero,$titular,$saldo);
        $this->saldo-= $apertura;
        $this->apertura=$apertura;
        $this->interes=$interes;
    }
    public function aplicaInteres(){
        $this->saldo+= ($this->saldo*$this->interes)/100;
    }
    public function mostrar(){
        return parent::mostrar()." apertura ".$this->apertura." interes ".$this->interes;
    }

}
?>