<?php 
class CuentaCorriente extends Cuenta{
    private $mantenimiento;
    public function __construct($numero,$titular,$saldo,$mantenimiento){
        parent::__construct($numero,$titular,$saldo);
        $this->saldo-=$mantenimiento;
        $this->mantenimiento=$mantenimiento;
    }

    public function reintegro($cantidad){
        if($this->saldo >$cantidad && $this->saldo >20){
            $this->saldo-=$cantidad;
            return true;
        }
        return false;
    }

    public function mostrar(){
        return parent::mostrar()." mantenimiento :".$this->mantenimiento;
    }
}
?>