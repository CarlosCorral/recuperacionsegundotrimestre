<?php 
class Cuenta{
    protected $numero,$titular,$saldo;
    public function __construct($numero,$titular,$saldo){
        $this->numero=$numero;
        $this->titular=$titular;
        $this->saldo=$saldo;
    }

    public function ingreso($cantidad){
        $this->saldo+=$cantidad;
    }

    public function reintegro($cantidad){
        if($this->sueldo>=$cantidad)
         $this->saldo -=$cantidad;
    }

    public function esPreferencial($cantidad){
        if($this->saldo > $cantidad){
            return true;
        }
        return false;
    }

    public function mostrar(){
        return "El numero de la cuenta es ".$this->numero." ,el titular de la cuenta es ".$this->titular." y su saldo es ".$this->saldo;
    }
}
?>