<?php 
include "Cuenta.php";
include "CuentaCorriente.php";
include "CuentaAhorro.php";

$cuentaCorriente = new CuentaCorriente(1,"Carlos",100,50);
$cuentaAhorro = new CuentaAhorro(2,"Maria",100,50,10);
echo $cuentaCorriente->mostrar();
echo "<br>".$cuentaAhorro->mostrar();
$cuentaAhorro->aplicaInteres();
echo "<br>".$cuentaAhorro->mostrar();
$cuentaCorriente->ingreso(100);
echo "<br>".$cuentaCorriente->mostrar();
$cuentaCorriente->reintegro(50);
echo "<br>".$cuentaCorriente->mostrar();
echo  "<br>".$cuentaCorriente->esPreferencial(150);
?>