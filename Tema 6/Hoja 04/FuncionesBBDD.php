<?php 
include_once 'conexionBD.php';

function getProductos(){
    $conexion=getConexion();
    $consulta = $conexion->prepare("SELECT * FROM productos inner join alimentaciones on productos.codigo=alimentaciones.codigo");
    if($consulta->execute()){
        while ($producto = $consulta->fetch()) {
            $productos[]=new Alimentacion($producto[0],$producto[1],$producto[2],$producto[8],$producto[5],$producto[6]);
        }
    $consulta = $conexion->prepare("SELECT * FROM productos inner join electronicas on productos.codigo=electronicas.codigo");
    if($consulta->execute()){
        while ($producto = $consulta->fetch()) {
            $productos[]=new Electronica($producto[0],$producto[1],$producto[2],$producto[7],$producto[5]);
        }
        unset($conexion);
        return $productos;
    }
}
}
?>