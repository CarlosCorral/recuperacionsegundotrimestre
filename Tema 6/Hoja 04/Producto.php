<?php 
abstract class Producto{
    protected $codigo,$precio,$nombre;
    public function __construct($codigo,$precio,$nombre){
        $this->codigo=$codigo;
        $this->precio=$precio;
        $this->nombre=$nombre;
    }
    public function mostrar(){
        return "Nombre :".$this->nombre." precio: ".$this->precio." codigo:".$this->codigo;
    }
    

    /**
     * Get the value of codigo
     */ 
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set the value of codigo
     *
     * @return  self
     */ 
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of precio
     */ 
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set the value of precio
     *
     * @return  self
     */ 
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }
}
?>