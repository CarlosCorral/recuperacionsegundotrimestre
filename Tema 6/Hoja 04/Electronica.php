<?php 
class Electronica extends Producto{
    private $garantia;

    public function __construct($codigo,$precio,$nombre,$garantia){
        parent::__construct($codigo,$precio,$nombre);
        $this->garantia=$garantia;
    }

    public function mostrar(){
        return parent::mostrar()." garantia ". $this->garantia;
    }
    /**
     * Get the value of garantia
     */ 
    public function getGarantia()
    {
        return $this->garantia;
    }

    /**
     * Set the value of garantia
     *
     * @return  self
     */ 
    public function setGarantia($garantia)
    {
        $this->garantia = $garantia;

        return $this;
    }
}
?>
