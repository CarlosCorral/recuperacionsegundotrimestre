<?php 
class Coche{
private $matricula;
private $velocidad;
public function __construct($matricula,$velocidad){
    $this->matricula=$matricula;
    $this->velocidad=$velocidad;
}

public function acelera($incremento){
    $total=$incremento+$this->velocidad;
    if($total>120){
    $this->velocidad=120;
    }else
    $this->velocidad=$total;
}

public function frena($decrecimiento){
    $total=$this->velocidad-$decrecimiento;
    if($total<0){
    $this->velocidad=0;
    }else
    $this->velocidad=$total;
}

public function getMatricula(){
    return $this->matricula;
}
public function getVelocidad(){
    return $this->velocidad;
}

}
?>