<?php 
class Monedero{
private $dinero;
private static $numero_monederos=0;
public function __construct($dinero){
    $this->dinero=$dinero;
    self::$numero_monederos++;
}
public function __destruct(){
    self::$numero_monederos--;
}

public function ingresar($cantidad){
    $this->dinero+=$cantidad;
}
public function extraer($cantidad){
    if($this->dinero>$cantidad)    
        $this->dinero-=$cantidad;
}
public function getDinero(){
    return $this->dinero;
}
public static function getNumeroMonederos(){
    return self::$numero_monederos;
}
}
?>