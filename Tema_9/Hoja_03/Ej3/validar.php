<?php 

function validarNombre($nombre){
if(strlen ($nombre)>4)
    return true;
return false;
}

function validarDNI($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni, 0, -1);
    if (strlen($dni)==9 && substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 )
      return true;
    return false;
  }

function validarPasswords($pass1,$pass2){
 if($pass1 == $pass2)
 return true;
return false;
}
$errores=[];

if(!validarNombre(($_POST["nombre"])))
    $errores["errorNombre"]="El nombre debe de tener mas de 4 caracteres";
if(!validarDNI(($_POST["dni"])))
    $errores["errorDNI"]="El DNI no es válido";
if(!validarPasswords($_POST["password1"],$_POST["password2"]))
    $errores["errorPassword"]="La contraseñas deben coincidir";
echo json_encode($errores);
?>