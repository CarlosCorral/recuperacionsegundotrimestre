<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script type="text/javascript" src="jquery.js"></script>

</head>
<body>
    <form method="POST">
        Nombre:<input type="text" name ="nombre" id="nombre"><br>
        DNI:<input type="text" name ="dni" id="dni"><br>
        PASSWORD:<input type="text" name ="password" id="password"><br>
        Confirmar PASSWORD:<input type="text" name ="password2" id="password2"><br>
        <input type="submit" name="boton"id="boton" value="Validar">
        <div id="errores" class="error">
    </form>
    <script type="text/javascript">
    	
        $(document).ready( function() {
            $("#boton").click(function(e){
            var validado=true;
            e.preventDefault();
            $("#errores").empty();

            $.ajax({
                type:"POST",
                url:"validar.php",
                dataType:"json",
                data:{
                        "nombre":$('#nombre').val(),
                        "dni":$('#dni').val(),
                        "password1":$('#password').val(),
                        "password2":$('#password2').val()
                },
                /*success: function(datos){
                    if(datos.errorNombre){
                        $("#errores").append(datos.errorNombre+"<br>");
                        validado=false;
                    }
                    if(datos.errorDNI){
                        $("#errores").append(datos.errorDNI+"<br>");
                        validado=false;
                    }
                    if(datos.errorPassword){
                        $("#errores").append(datos.errorPassword+"<br>");
                        validado=false;
                    }
                    if(validado){
                        $("#datos").submit;
                    }
                }*/
                success: function(datos){
                    let errores = new Array();
                    if(datos.errorNombre){
                        errores.push(datos.errorNombre+"\n");
                        validado=false;
                    }
                    if(datos.errorDNI){
                        errores.push(datos.errorDNI+"\n");
                        validado=false;
                    }
                    if(datos.errorPassword){
                        errores.push(datos.errorPassword+"\n");
                        validado=false;
                    }
                    if(validado==false){
                        alert(errores);
                    }
                }
            });
        });
    });
    </script>
</body>
</html>