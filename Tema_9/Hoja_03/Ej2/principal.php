<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script type="text/javascript" src="jquery.js"></script>
    <script type="text/javascript">
    	$(document).ready( function() {
            $("#boton").click(function(){
                $("#div").load("texto.txt");
			});
            $("#boton2").click(function(){
                $("#div").load("texto.PHP");
			});
        });
    </script>
</head>
<body>
    <input type="button" id="boton" value="boton txt">
    <input type="button" id="boton2" value="boton php">
    <div id="div"></div>
</body>
</html>