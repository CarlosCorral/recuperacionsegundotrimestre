@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    <div class="col-sm-9">
        Nombre:{{$cuidador->nombre}}<br>
        <ul>
        @foreach($cuidador->titulaciones() as $titulacion )
        <li> <a href="{{ route('titulaciones.show' , $titulacion ) }}">
       {{$titulacion->nombre}}
        </a></li>
        @endforeach
        </ul>
        <a type="button" class="btn btn-secondary"  href="{{ route('animales.index') }}">Volver</a>
    </div>
</div>
@endsection