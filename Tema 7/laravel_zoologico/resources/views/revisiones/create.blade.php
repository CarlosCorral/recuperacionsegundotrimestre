@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
            Añadir revision a {{$animal->especie}}
            </div>
            <div class="card-body" style="padding:30px">
            <form method='post' action="{{route('revisiones.store',$animal) }}">
            @csrf
 
                <label for="fecha">Fecha de la revisión</label>
                <input type="date" name="fecha" class="form-control" required>
                </div>
 
                <div class="form-group">
                     <label for="descripcion">Descripción</label>
                     <textarea name="descripcion" id="descripcion" class="form-control" rows="3"></textarea>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
                      Añadir Revision
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection