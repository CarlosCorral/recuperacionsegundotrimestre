@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    <div class="offset-md-3 col-md-6">
        <div class="card">
            <div class="card-header text-center">
            Modificar animal
            </div>
            <div class="card-body" style="padding:30px">
            <form method='post' action="{{ route('animales.update', $animal) }}">
            @csrf
            @method('put')
                <div class="form-group" >
                <label for="especie">Especie</label>
                <input type="text" name="especie" id="especie" class="form-control" value="{{$animal->especie}}" required>
                </div>

                <div class="form-group" >
                <label for="peso">Peso</label>
                <input type="number" name="peso" class="form-control" value="{{$animal->peso}}" required>
                </div>

                <div class="form-group">
                <label for="altura">Altura</label>
                <input type="number" name="altura" class="form-control" value="{{$animal->altura}}" required>
                </div>

                <div class="form-group">
                <label for="fechaNacimiento">Fecha de Nacimiento</label>
                <input type="date" name="fechaNacimiento" class="form-control" value="{{$animal->fechaNacimiento}}" required>
                </div>

                <div class="form-group">
                <label for="alimentacion">Alimentacion</label>
                <input type="text" name="alimentacion" class="form-control" value="{{$animal->alimentacion}}" required>
                </div>
 
                <div class="form-group">
                     <label for="descripcion">Descripción</label>
                     <textarea name="descripcion" id="descripcion" class="form-control" rows="3" >{{$animal->descripcion}}</textarea>
                </div>
                <div class="form-group">
                <input type='file' name="imagen">
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
                      Modificar animal
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection