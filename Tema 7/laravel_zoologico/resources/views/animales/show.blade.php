@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    <div class="col-sm-3">
        <img src="{{asset('Imagenes')}}/{{$animal->imagen}}" style="width:320px"/>
    </div>
    <div class="col-sm-9">
        Especie:{{$animal->especie}}<br>
        Peso:{{$animal->peso}}<br>
        Altura:{{$animal->altura}}<br>
        FechaNacimiento:{{$animal->fechaNacimiento}}<br>
        Alimentacion:{{$animal->alimentacion}}<br>
        Descripcion:{{$animal->descripcion}}<br>
        Revisiones:<br>
        <ul>
        @foreach ($animal->revisiones as $revision)
         <li>{{$revision->fecha}}: 
        {{$revision->descripcion}}</li>
        @endforeach
        </ul>
        Cuidadores:
        <ul>
        @foreach($animal->cuidadores as $cuidador )
        <li>{{$cuidador->nombre}}</li>
        @endforeach
        <ul>
        <a type="button" class="btn btn-primary" href="{{ route('animales.edit' , $animal ) }}">Editar</a>
        <a type="button" class="btn btn-secondary"  href="{{ route('animales.index') }}">Volver</a>
        <a type="button" class="btn btn-secondary"  href="{{ route('revisiones.create' , $animal) }}">Añadir revision</a>
    </div>
</div>
@endsection