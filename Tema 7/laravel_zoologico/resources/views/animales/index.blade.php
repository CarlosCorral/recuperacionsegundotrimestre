@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    @foreach( $arrayAnimales as $animal )
        <div class="col-xs-12 col-sm-6 col-md-4 ">
        <a href="{{ route('animales.show' , $animal ) }}">
            <img src="{{asset('Imagenes')}}/{{$animal->imagen}}" style="height:200px"/>
            <h4 style="min-height:45px;margin:5px 0 10px 0">
                {{$animal->especie}}
            </h4>
        </a>
        Cuidadores:
        <ul>
        @foreach($animal->cuidadores as $cuidador )
        <a href="{{ route('cuidadores.show' , $cuidador ) }}">
        <li>{{$cuidador->nombre}}</li>
        </a>
        @endforeach
        <ul>
        
        </div>
    @endforeach
</div>

@endsection