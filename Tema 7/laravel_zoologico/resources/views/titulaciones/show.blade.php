@extends('layouts.master')
@section('titulo')
    Zoológico
@endsection
@section('contenido')
<div class="row">
    <div class="col-sm-9">
        <h2>Titulación: {{$titulacion->nombre}}</h2>
        Cuidadores: 
        <ul>
        @foreach($titulacion->cuidadores as $cuidador )
            <li>{{$cuidador->nombre}}</li>
        @endforeach
        </ul>
        <a type="button" class="btn btn-secondary"  href="{{ route('animales.index') }}">Volver</a>
    </div>
</div>
@endsection