<?php

namespace Database\Seeders;

use App\Models\Revision;
use App\Models\Animal;
use Illuminate\Database\Seeder;

class RevisionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r = new Revision();
        $r->animal_id=Animal::first()->id;
        $r->fecha="2021-12-4";
        $r->descripcion="Come poco";

        $r2 = new Revision();
        $r2->animal_id=Animal::first()->id;
        $r2->fecha="2021-12-6";
        $r2->descripcion="Hinchado";
    
        $r->save();
        $r2->save();
    }
}
