<?php

namespace Database\Seeders;

use App\Models\Titulacion;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class TitulacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titulaciones=["Licenciado","Veterinario","Alimentacion"];
        foreach($titulaciones as $titulacion){
        $t = new Titulacion();
        $t->nombre = $titulacion;
        $t->slug = Str::slug($t->nombre);
        $t->save();
        }
    }
}
