<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\Models\Animal;
use Illuminate\Http\Request;

class RestWebServiceController extends Controller
{
    public function index(){
        $animales=Animal::all();
        return response()->json($animales);
    }
    public function show(Animal $animal){
        return response()->json($animal);
    }
    public function destroy(Animal $animal){
        $animal->delete();
        return response()->json(["mensaje"=>"Se ha borrado el animal ".$animal->especie]);
    }
    public function store(Request $request){
        $animal=new Animal();
        $animal->especie=$request->especie;
        $animal->peso=$request->peso;
        $animal->altura=$request->altura;
        $animal->fechaNacimiento=$request->fechaNacimiento;
        $animal->alimentacion=$request->alimentacion;
        $animal->descripcion=$request->descripcion;
        $animal->slug=Str::slug($request->especie);
        $animal->save();
        return response()->json(["mensaje"=>"Animal{$animal}"]);
    }
}
