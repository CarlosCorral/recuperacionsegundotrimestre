<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Animal;
use App\Models\Revision;
class RevisionController extends Controller
{
   public function crear(Animal $animal){
      return view('revisiones.create',['animal'=>$animal]);
   }

   public function store(Animal $animal,Request $request){
      $revision = new Revision();
      $revision->animal_id=$animal->id;
      $revision->fecha=$request->fecha;
      $revision->descripcion=$request->descripcion;
      $revision->save();
      return view('animales.show',['animal'=>$animal]);
   }
}
