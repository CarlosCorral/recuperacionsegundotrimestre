<?php

namespace App\Http\Controllers;

use App\WebServices\WSDLDocument;
use Illuminate\Http\Request;
use SoapServer;
use App\WebServices\ZoologicoWebService;

class SoapServerController extends Controller
{
    private $clase = ZoologicoWebService::class;
    private $uri="http://zoologico.com/api";
    private $urlWSDL="http://zoologico.com/api/wsdl";

    public function getServer(){
        $server= new SoapServer($this->urlWSDL);
        $server->setClass($this->clase);
        $server->handle();
        exit;
    }
    public function getWSDL(){
        $wsdl=new WSDLDocument($this->clase,$this->uri,$this->uri);
        $wsdl->formatOutPut=true;
        header('Content-Type: text/xml');
        echo $wsdl->saveXML();
        exit;
    }
}
