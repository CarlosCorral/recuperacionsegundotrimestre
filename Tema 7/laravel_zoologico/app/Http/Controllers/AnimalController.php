<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Animal;
class AnimalController extends Controller
{
   public function index(){
	$animales=Animal::all();
    return view('animales.index',['arrayAnimales'=>$animales]);
    }

    public function show(Animal $animal){
    return view('animales.show', ['animal'=>$animal]);
    }

    public function create(){
    return view('animales.create');
    }

    public function edit(Animal $animal){
    return view('animales.edit', ['animal'=>$animal]);
    }
    public function store(Request $request){
        $animal=new Animal();
        $animal->especie=$request->especie;
        $animal->peso=$request->peso;
        $animal->altura=$request->altura;
        $animal->fechaNacimiento=$request->fechaNacimiento;
        $animal->alimentacion=$request->alimentacion;
        $animal->descripcion=$request->descripcion;
        $animal->slug=Str::slug($request->especie);
        $animal->save();
        return view('animales.show', ['animal'=>$animal]);
    }

    public function update(Request $request,Animal $animal){
        $animal->especie=$request->especie;
        $animal->peso=$request->peso;
        $animal->slug=Str::slug($request->especie);
        $animal->altura=$request->altura;
        $animal->fechaNacimiento=$request->fechaNacimiento;
        $animal->alimentacion=$request->alimentacion;
        $animal->descripcion=$request->descripcion;
        $animal->save();
        return view('animales.show', ['animal'=>$animal]);
    }

    public function buscar(Request $request){
        //$animales=Animal::all()->where("especie","like","%".$request->buscador."%")->pluck("especie");
        $animales=Animal::where("especie","like","%".$request->buscador."%")->pluck("especie");
        return response()->json($animales);
    }
    
}
