<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
/**
 * @property string $especie
 * @property string $altura
 * @property string $peso
 * @property string $alimentacion
 * @property string $fechaNacimiento
 */
class Animal extends Model
{
    use HasFactory;
    protected $table="animales";
    
    public function getEdad()
    {
        $fechaFormateada=Carbon::parse($this->fechaNacimiento);
        return $fechaFormateada->diffInYears(Carbon::now());
    }
    public function revisiones()
    {
    return $this->hasMany(Revision::class);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';  
    }

    public function cuidadores()
    {
    return $this->belongsToMany(Cuidador::class);
    }
   

}
