<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Titulacion extends Model
{
    use HasFactory;
    protected $table="titulaciones";

    public function cuidadores()
    {
    return $this->hasMany(Cuidador::class, 'titulacion1_id')->orWhere('titulacion2_id',$this->id);
    }
}
