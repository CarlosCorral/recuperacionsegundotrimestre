<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuidador extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';  
    }
    use HasFactory;
    protected $appends  = ["titulaciones"];
    protected $table="cuidadores";

    public function animales()
    {
    return $this->belongsToMany(Animal::class);
    }
   
    public function titulaciones()
    {
    
        return $this->belongsTo(Titulacion::class,'titulacion1_id')->get()
        ->merge($this->belongsTo(Titulacion::class,'titulacion2_id')->get());
    }

}
