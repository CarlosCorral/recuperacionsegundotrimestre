<?php
namespace App\WebServices;
use App\Models\Animal;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class ZoologicoWebService{
/**
 * @param string $correo
 * @param string $pass
 * @return boolean
 */
public function login($correo,$pass){
    return Auth::attempt(['email' => $correo, 'password'=>$pass]);
}
/**
 * Undocumented function
 *
 * @param int $id
 * @return App\Models\Animal
 */
public function getAnimal($id){
$animal =Animal::find($id);
return $animal;
}
/**
 * Undocumented function
 *
 * @return App\Models\Animal[]
 */
public function getAnimales(){
    $animales=Animal::all();
    return $animales;
}
/**
 * Undocumented function
 *
 * @param string $alimentacion
 * @return App\Models\Animal[]
 */
public function getAnimalesAlimentacion($alimentacion){
$animales=Animal::all()->where("alimentacion",$alimentacion);
return $animales;
}
/**
 * Undocumented function
 *
 * @param string $especie
 * @return App\Models\Animal[]
 */
public function busqueda($especie){
    $animales=Animal::where("especie","like","%".$especie."%")->get();
    return $animales;
}
}
?>