<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('generos')->delete();
        $this->call(GeneroSeeder::class);

        DB::table('artistas')->delete();
        $this->call(ArtistasSeeder::class);

        DB::table('obras')->delete();
        $this->call(ObrasSeeder::class);
    }
}
