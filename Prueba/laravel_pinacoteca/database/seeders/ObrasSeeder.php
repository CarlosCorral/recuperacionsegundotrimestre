<?php

namespace Database\Seeders;

use App\Models\Artista;
use App\Models\Obra;
use Illuminate\Database\Seeder;

class ObrasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $obras=["La casa de Bernarda Alba","Soy leyenda","Crónica de una muerte anunciada","Un heroe","Don Quijote","Carlota se va","El tocador de señoras"];
        foreach($obras as $obra){
        $o= new Obra(); 
        $o->nombre=$obra;
        $o->artista_id=Artista::all()->random()->id;
        $o->imagen="libro.png";
        $o->save();
        }
    }
}
