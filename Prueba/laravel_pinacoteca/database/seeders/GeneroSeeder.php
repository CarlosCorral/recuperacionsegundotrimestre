<?php

namespace Database\Seeders;

use App\Models\Genero;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class GeneroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generos=["terror","romance","comedia","drama","misterio","bibliografia"];
        foreach($generos as $genero){
        $g = new Genero();
        $g->nombre = $genero;
        $g->slug = Str::slug($genero);
        $g->save();
        }
    }
}
