<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtistas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artistas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('pais');
            $table->date('fechaNacimiento');
            $table->string('slug')->unique();
            $table->unsignedBigInteger("genero1_id");
            $table->foreign('genero1_id')->references("id")->on("generos");
            $table->unsignedBigInteger("genero2_id");
            $table->foreign('genero2_id')->references("id")->on("generos");
            $table->unsignedBigInteger("genero3_id");
            $table->foreign('genero3_id')->references("id")->on("generos");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artistas');
    }
}
