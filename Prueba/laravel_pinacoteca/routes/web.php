<?php

use App\Http\Controllers\ArtistaController;
use App\Http\Controllers\GeneroController;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\ObraController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class,'inicio']);

Route::get('/artistas', [ArtistaController::class,'index'])->name('artistas.index');

Route::get('/artistas/ver/{artista}', [ArtistaController::class,'mostrar'])->name('artistas.show');

Route::get('/genero/{genero}', [GeneroController::class,'mostrar'])->name('genero.show');

Route::get('/obra/crear', [ObraController::class,'crear'])->name('obras.create');

Route::post('/obra/crear', [ObraController::class,'store'])->name('obras.store');

