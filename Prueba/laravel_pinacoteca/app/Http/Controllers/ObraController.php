<?php

namespace App\Http\Controllers;

use App\Models\Artista;
use App\Models\Obra;
use Illuminate\Http\Request;

class ObraController extends Controller
{
    public function store(Request $request){
        $obra = new Obra();
        $obra->nombre=$request["nombre"];
        $obra->imagen=$request["imagen"];
        $obra->artista_id=$request["artista"];
        $obra->save();
        return  redirect()->action([ArtistaController::class,'index']);
    }
    public function crear(){
    $artistas=Artista::all();
    return view('obras.crear',['arrayArtistas'=>$artistas]);
    }
}
