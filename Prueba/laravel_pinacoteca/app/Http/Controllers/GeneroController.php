<?php

namespace App\Http\Controllers;

use App\Models\Genero;
use Illuminate\Http\Request;

class GeneroController extends Controller
{
    public function mostrar(Genero $genero){
        return view('generos.mostrar', ['genero'=>$genero]);
    }
}
