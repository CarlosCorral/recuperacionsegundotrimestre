<?php

namespace App\Http\Controllers;

use App\Models\Artista;
use Illuminate\Http\Request;

class ArtistaController extends Controller
{
    public function index(){
    $artistas=Artista::all();
    return view('artistas.index',['arrayArtistas'=>$artistas]);
    }

    public function mostrar(Artista $artista){
        return view('artistas.mostrar', ['artista'=>$artista]);
    }
}
