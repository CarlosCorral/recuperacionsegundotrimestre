<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    use HasFactory;
    protected $table="generos";

    public function getRouteKeyName()
    {
        return 'slug';  
    }

    public function artistas()
    {
    return $this->hasMany(Artista::class, 'genero1_id')->orWhere('genero2_id',$this->id)->orWhere('genero3_id',$this->id);
    }
}
