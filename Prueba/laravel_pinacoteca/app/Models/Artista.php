<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    use HasFactory;
    protected $table="artistas";
    
    public function generos()
    {
    
        return $this->belongsTo(Genero::class,'genero1_id')->get()
        ->merge($this->belongsTo(Genero::class,'genero2_id')->get())
        ->merge($this->belongsTo(Genero::class,'genero3_id')->get());
    }

    public function obras()
    {
    return $this->hasMany(Obra::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';  
    }
}
