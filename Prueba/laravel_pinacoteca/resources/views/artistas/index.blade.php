
<table>
<th>Nombre</th><th>Pais</th><th>Obras</th>
@foreach( $arrayArtistas as $artista )
    <tr>
    <td>
        <a href="{{ route('artistas.show' , $artista ) }}">
                {{$artista->nombre}}   
        </a>
    </td>
    <td>
    {{$artista->pais}}
    </td>
    <td>
        Tiene :{{$artista->obras->count()}} obras
    </td>
    </tr>
    @endforeach
</table>
