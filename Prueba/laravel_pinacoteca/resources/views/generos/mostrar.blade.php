<h2>Generos: {{$genero->nombre}}</h2>
        Obras: 
        <ul>
        @foreach($genero->artistas as $artista )
            @foreach($artista->obras as $obra )
                <li>{{$obra->nombre}}</li>
            @endforeach
        @endforeach
        </ul>
</div>