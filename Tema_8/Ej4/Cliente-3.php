<?php 
$animal =array(
    'especie' => 'jaoso',
    'peso' => 5000,
    'altura' => 400,
    'fechaNacimiento' => '2011-07-07',
    'imagen' => 'elefante.jpg',
    'alimentacion' => 'herbívoro',
    'descripcion' => 'El elefante africano es el mayor mamífero terrestre. Viven en una sociedad matriarcal de estructura muy compleja, dirigida siempre por la hembra más vieja y por lo general estéril, que tiene a su cargo a las hembras adultas y crías menores de 14 años, a quienes no duda en proteger de todo peligro.

Los grupos de elefantes se desplazan siempre en fila india, siguiendo un orden preestablecido según la jerarquía de cada miembro, y se moverán de un punto a otro dependiendo de las decisiones de la matriarca, que es quien dicta cuándo y a dónde dirigirse. Los elefantes se comunican contínuamente a través de diferentes tipos de señales, algunas de ellas imperceptibles para el hombre por su baja frecuencia, pero que éstos pueden escuchar a más de 10 kilómetros de distancia. El nacimiento de una nueva cría es un acontecimiento importanta para la manada: ésta, además de por su madre, será cuidada por el grupo.

En estaciones secas, los elefantes llegan a formar grupos de 300 individuos, lo que provoca que sus migraciones devasten lo que encuentran a su paso. Sin embargo, van dejando en sus heces estiércol con semillas sin digerir que germinarán, convirtiéndose así en los mejores regeneradores de la selva.

La trompa del elefante es un apéndice nasal compuesto de 4.000 músculos y tendones, lo que le proporciona una fuerza y precisión extraordinarias, siendo capaces de levantar con ella hasta una tonelada de peso. En ella, los elefantes adultos pueden albergar 17 litros de agua para bañarse o beber.

En las orejas poseen gran cantidad de venas, por las cuales hacen circular 680 litros de sangre en 20 minutos, refrigerando así su cuerpo. No es raro en absoluto verlos en Cabárceno mojarse las orejas con barro o agua para ayudar a esta refrigeración.

El desgaste de los colmillos indica la lateralidad de estos animales: si el más desgastado es el derecho, el elefante es diestro.

La esperanza de vida de un elefante es de 62 años, y hasta su muerte ni sus colmillos ni su cuerpo dejarán de crecer.'
);
$url="http://zoologico.com/rest/insertar";
$curl= curl_init($url);
curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"POST");
curl_setopt($curl,CURLOPT_POSTFIELDS,$animal);
curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
$respuesta_curl = curl_exec($curl);
curl_close($curl);
$respuesta_decodificada= json_decode($respuesta_curl);
var_dump($respuesta_decodificada);
?>